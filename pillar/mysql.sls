mysql:
  global:
    client-server:
      default_character_set: utf8

  clients:
    mysql:
      default_character_set: utf8
    mysqldump:
      default_character_set: utf8
  
  server:
    mysqld:
      bind-address: 0.0.0.0
      port: 3307
      binlog-ignore-db:
         - mysql
         - sys
         - information_schema
         - performance_schema
