nginx:
  servers:
    config:
      - listen:
        - 80
      - index: 
        - index.html
        - index.htm
      - pid:
        - /run/nginx.pid
