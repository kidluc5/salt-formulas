redis:
  root_dir: /var/lib/redis
  port: 6379
  bind: 127.0.0.1

  lookup:
    svc_state: running
    cfg_name: /etc/redis.conf
    pkg_name: redis-server
    svc_name: redis-server
    overcommit_memory: True
