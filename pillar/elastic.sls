elasticsearch:
  version: 5.4.3
  config:
    network.host: 127.0.0.1
    http.port: 9200
    path.data: /srv/data
    path.logs: /srv/logs
  sysconfig:
    ES_STARTUP_SLEEP_TIME: 5
    ES_HEAP_SIZE: 8g
    MAX_OPEN_FILES: 65535
  plugins:
    lang-python: lang-python
    kopf: lmenezes/elasticsearch-kopf
