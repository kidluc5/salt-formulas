apt-transport-https:
  pkg:
    - installed

elasticsearch_repo:
  pkgrepo.managed:
    - humanname: Elasticsearch Debian
    - name: deb https://artifacts.elastic.co/packages/5.x/apt stable main
    - dist: stable
    - file: /etc/apt/sources.list.d/elasticsearch-5.x.list
    - keyid: D88E42B4
    - keyserver: keyserver.ubuntu.com
    - clean_file: true
    - enabled: 1
    - gpgcheck: 1
    - gpgkey: http://artifacts.elastic.co/GPG-KEY-elasticsearch\

command:
  cmd.run:
    - name: sudo apt-get update

elasticsearch:
  pkg:
    - installed

service:
  cmd.run:
    - name: sudo service elasticsearch start
