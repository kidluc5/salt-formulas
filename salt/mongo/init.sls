mongodb_repo:
  pkgrepo.managed:
    - humanname: MongoDB.org Reposity
    - name: deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse
    - file: /etc/apt/sources.list.d/mongodb-org.list
    - keyid: 0C49F3730359A14518585931BC711F9BA15703C6
    - keyserver: keyserver.ubuntu.com

command:
  cmd.run:
    - name:
      - sudo apt-get udpate

mongodb-org:
  pkg:
    - installed
#    pkgs:
#      - mongodb-org
#      - mongodb-org-server
#      - mongodb-org-mongos
#      - mongodb-org-shell
#      - mongodb-org-tools

service:
  cmd.run:
    - name: sudo service mongod start
